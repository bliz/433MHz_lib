#
// Yet another HOLTEK decoder - this time for WHA1 and WHA2 alarm PIR (and presumably other related peripherals), which are based on a Holtek microcontroller in the same family, but running at 1MHz, with bespoke firmware.

// Based on http://www.holtek.com/pdf/Encoder_Decoder/HT6P2x5Av100.pdf
//
// Holtek ASK 300 -450MHz band Tx family decoder
//
// According to the HT6P2x54 datasheet, a message consists of pilot of 23 clocks,
// followed by "1/3 bit" i.e. one clock transition from the low to high to low, however watching the output of the WHA2 PIR on the scope suggested this device was different.
//
// The above datasheets shows this as 23 clocks of logic low... so our message should start with "silence" for ...
// 23 x clock wavelength (λ) + one high of "1/3 bit" - i.e. one clock length(λ)) consisting of from low to high to low transition.
//
// From the datasheet this is described thus...
//
// Section 1.4  - Synchronous Bit Waveform For the HT6P2x5A device, a synchronous bit waveform is 8-bits long.
//                It exhibits a low pulse for 23λ followed by a high pulse for 1λ ...
//
// The "Syncronous bit" is followed by address bits A0 - A19 (60 clocks (60 x λ since we have 3 clocks per bit))
// This is then followed by D3 - D0 (12 clocks)
// This in turn is followed by a 4 bit "anti-code" in other words the bits "0101" (a further 12 λ at 3λ per bit)
//
// Message frame is therefore 23+1+(20*3)+(4*3)+(4*3) clocks long i.e. 108 clocks
//
// Every message bit consists of a low to high transition, followed by a high to low transition, so we expect 2 (CHANGE) interrupts per bit.
//
// Every message frame therefore consists of 2 transistions (start bit) + 40 (address bit transitions) + 8 (data bit transitions) + 8 (anticode transitions for the 4 bits of 1010)
//
// Every valid message frame is 58 transitions long, and lasts for 108 clocks.
//
// Bit “1” consists of a “low” pulse for 2λ then changes to a “high” pulse for 1λ.
// Bit “0” consists of a “low” pulse for 1λ then changes to a “high” pulse for 2λ.
//
// Our state machine therefore needs to have the following states.
//
// 0 - Listening for start bit
//
// 1 - Looking for the pilot or "synchronous bit" - a low period (our start bit), followed by a high transition (at the end of the start bit), followed by a low transition (the end of the sync bit), where the low period (start bit) is >23 times longer
//     than the high period (sync bit). This high period is by definition (λ)
//
// 2 - Looking for the next 28 bits (20 message + 4 address + 4 "anticode" bits)  taking in 56  transitions (each bit is low to high, then high to low since it is the length
//     rather than the level which determines whether the bit is a 0 or a 1
//
//     This state should last for 84 clock cycles or 84(λ) +/- any clock drift.
//
// 3 - Decoding the address and data
//
// and so back to 1
//
//
//
// Icing on this particular cake is the ability to consistently derive the clock from state 1 thus rendering the code, clock agnostic, since there are
// a number of different members of this decoder family with different clock arrangements, varying from 1MHz resonators to 1KHz R/C or internal oscillators.
// More icing is the ability to allow for different message formats.
//
// Althoug all of the manufacturer declared variants use a 24 bit message, this particlar device appears to send only 16 bits, there may be other variants
// that work differently. There are application notes that declare "encode 24 or 28 bits of address and data", so other Holtek in house variations also exist.
// 
